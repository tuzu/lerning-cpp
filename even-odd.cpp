#include <iostream>
#include <vector>

int main(){

// Creating variables.
std::vector<int> num = {1, 2, 3, 4 ,5 ,6 ,7 ,8 ,9 ,10 ,11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
int even_total = 0;
int odd_total = 1;

for(int i = 0; i < num.size(); i++){

if (num[i] % 2 == 0){
even_total = even_total + num[i];
}
else{
odd_total = odd_total * num[i];
}
}

std::cout << "Sum of even numbers is " << even_total << ".\n";
std::cout << "Product of odd numbers is " << odd_total << ".\n";

}
