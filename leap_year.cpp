#include <iostream>

int main() {

// seting variables
int year;
std::cout << "plese enter your year: ";
std::cin >> year;

// body of program
if (year < 10000 && year > 999) {
	if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {
		std::cout << "Year " << year << " is leap year.\n";
	}
	else {
		std::cout << "Year " << year << " is not a leap year \n";
	}
}
else {
	std::cout << "Year must be 4 digit number\n";
}
}
