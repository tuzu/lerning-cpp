# Just my cheat sheat.

## Warning this cheatsheet is moastly copy and paste from [codecademy](https://www.codecademy.com/learn/learn-c-plus-plus/modules/learn-cpp-hello-world/cheatsheet)

## Basic info

file extension is .cpp

program start with: init main() 
{

*Here whrite your code*
}

Its also good to: *#include \<iostream>* for input and output

do one line coments with *//*

and multi line coments with /* Here whrite your coment */

## Inpuit and output

*std::cout << "Some text";*  output text to console

*std::cin >> variable;* store user input to variable

\n make new line (put in "" in std::cout)

example:

```cpp
#include <iostream>

int main() {

  std::cout << "Hello, world!\n";
  std::cout << "Hello world, on other line!\n";
  std::cout << "Hello world, on same line!";

}
```

## Variables

variable is somthink that can store data (in RAM)

posible math operations are : + - * / %

Variable types are :

- init (stors whole number from -2³¹ to 2³¹-1, requires 4 bytes of RAM)
- double (for floating numbers, uses 8 bytes of RAM)
- char (for one characters put charcters inside of '', need 1 byte of RAM)
- std::string (for storing test string, put yours text inside of "")
- bool (stors true or false, uses 1 byte of RAM)

```cpp
#include <iostream>

int main() {

 double weghit_heart;
 double weghit_mars;
 std::string unit;

  // Ask the user
  std::cout << "Enter unit: ";
  std::cin >> unit;
  std::cout << "\n";

  std::cout << "Enter weghit on earth: ";
  std::cin >> weghit_heart;
  std::cout << "\n";

  weghit_mars = weghit_heart * 0.38;

  std::cout << "Weghit on mars is " << weghit_mars << unit <<"\n";

}
```

# if and else
if means if somthink is true run the code inside if
example"
```cpp
if (a == 10) {
  // Code goes here
}
```

else rans if if is false
example:
```cpp
if (year == 1991) {
  // This runs if it is true
}
else {
  // This runs if it is false
}
```
I switch dont know how to explain this so here is example:
```cpp
switch (grade) {
  case 9:
    std::cout << "Freshman\n";
    break;
  case 10:
    std::cout << "Sophomore\n";
    break;
  case 11:
    std::cout << "Junior\n";
    break;
  case 12:
    std::cout << "Senior\n";
    break;
  default:
    std::cout << "Invalid\n";
    break;
}
```
Else if is (I think) self explenatory, here is a example:
```cpp
if (apple > 8) {
  // Some code here
}
else if (apple > 6) {
  // Some code here
}
else {
  // Some code here
}
```
